package xproject.controller.account;

import xproject.service.Main;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.sql.SQLException;
import net.miginfocom.swing.MigLayout;

public class AccountGui extends JPanel {
    private static final int CREATE = 5;
    private static final int LOGIN = 2;

    private static final String[] FIELD_NAMES = {
        "Account Name",
        "Password",
        "First Name",
        "Last Name",
        "Email"
    };

    private JLabel[] labels = new JLabel[FIELD_NAMES.length];

    private JTextField[] textFields = new JTextField[FIELD_NAMES.length];

    private JButton createButton = new JButton("Create Account");
    private JButton loginButton = new JButton("Login");
    private JButton backButton = new JButton("Back");

    private AccountJpa jpa = new AccountJpa();

    public AccountGui() {
        for (int i = 0; i < FIELD_NAMES.length; i++) {
            labels[i] = new JLabel(FIELD_NAMES[i]);
            textFields[i] = new JTextField(30);
        }

        add(initPanel(LOGIN), BorderLayout.CENTER);

        loginButton.addActionListener(new ButtonHandler());
        createButton.addActionListener(new ButtonHandler());
        backButton.addActionListener(new ButtonHandler());
    }

    private JPanel initPanel(int numOfFields) {
        JPanel panel = new JPanel();
        panel.setLayout(new MigLayout());

        for (int i = 0; i < numOfFields; i++) {
            panel.add(labels[i], "align label");
            panel.add(textFields[i], "wrap");
        }

        if (numOfFields == LOGIN) {
            panel.add(loginButton);
            panel.add(createButton);
        } else {
            panel.add(createButton);
            panel.add(backButton);
        }

        return panel;
    }

    private boolean isEmptyFieldData(int numOfFields) {
        boolean flag = false;

        for (int i = 0; i < numOfFields; i++) {
            if (textFields[i].getText().trim().isEmpty()) {
                flag = true;
            }
        }

        return flag;
    }

    private Account getAccountInfo(int numOfFields) {
         Account account = new Account();

        for (int i = 0; i < numOfFields; i++) {
            switch (i) {
                case 0:
                    account.setAccountName(textFields[i].getText().trim());
                    break;
                case 1:
                    account.setPassword(textFields[i].getText().trim());
                    break;
                case 2:
                    account.setFirstName(textFields[i].getText().trim());
                    break;
                case 3:
                    account.setLastName(textFields[i].getText().trim());
                    break;
                case 4:
                    account.setEmail(textFields[i].getText().trim());
                    break;
            }
        }

        return account;
    }

    private void submitAccountInfo(int numOfFields) {
        if (isEmptyFieldData(numOfFields)) {
            JOptionPane.showMessageDialog(null, "Error");
            return;
        }

        if (numOfFields == CREATE) {
            jpa.createAccount(getAccountInfo(numOfFields));
            JOptionPane.showMessageDialog(null, "Creation Sucessfull!");
        } else {
            jpa.loginAccount(getAccountInfo(numOfFields));
            JOptionPane.showMessageDialog(null, "Login Successfull!");
        }
    }

    private void changePanel(JPanel newPanel) {
        removeAll();
        add(newPanel, BorderLayout.CENTER);
        validate();
        repaint();
    }

    private class ButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "Login":
                    submitAccountInfo(LOGIN);
                    break;
                case "Create Account":
                    changePanel(initPanel(CREATE));
                    createButton.setText("Create");
                    break;
                case "Create":
                    submitAccountInfo(CREATE);
                    break;
                case "Back":
                    changePanel(initPanel(LOGIN));
                    createButton.setText("Create Account");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Invalid command");
            }
        }
    }
}
