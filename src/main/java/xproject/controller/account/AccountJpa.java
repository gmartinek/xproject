package xproject.controller.account;

import xproject.controller.ControllerJpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;;

public class AccountJpa {

    private static final EntityManagerFactory emf = ControllerJpa.getEntityManagerFactory();
    private Account account = null;

    public void createAccount(Account account) {
        EntityManager em = null;
        EntityTransaction tx = null;

        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            em.persist(account);
            tx.commit();
            this.account = account;
        } catch (RuntimeException e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            throw e;
        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }
    }

    public void loginAccount(Account account) {
        EntityManager em = null;

        try {
            em = emf.createEntityManager();
            this.account = em.find(Account.class, account.getAccountName());
        } catch (RuntimeException e) {
            throw e;
        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }
    }
}
