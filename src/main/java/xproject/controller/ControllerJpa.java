package xproject.controller;

import java.util.Properties;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class ControllerJpa {

    public static EntityManagerFactory emf;

    public static EntityManagerFactory getEntityManagerFactory() {
        if (emf == null) {
            Properties props = new Properties();
            props.setProperty("eclipselink.persistencexml", "META-INF/persistence.xml");
            emf = Persistence.createEntityManagerFactory("default", props);
        }

        return emf;
    }
}
