package xproject.controller.account;

import xproject.controller.JpaTestHelper;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import org.junit.Before;
import org.junit.Test;

public class TestAccount {

    private static final EntityManagerFactory emf = JpaTestHelper.getEntityManagerFactory();
    private Account account = new Account();

    @Before
    public void setConditions() {
        JpaTestHelper.resetDatabase();
        account.setAccountName("Test");
        account.setPassword("password");
        account.setFirstName("First");
        account.setLastName("Last");
        account.setEmail("foo@bar.com");
    }

    @Test
    public void testAccountCreate() {
        EntityManager em = null;
        EntityTransaction tx = null;

        try {
            System.out.println("Test Create");
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            em.persist(account);
            tx.commit();
            System.out.println("Create Successful");
        } catch (RuntimeException e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            throw e;
        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }
    }

    @Test
    public void testAccountLogin() {
        EntityManager em = null;
        Account login = null;

        try {
            System.out.println("Test Login");
            em = emf.createEntityManager();
            login = em.find(Account.class, account.getAccountName());
            System.out.println("Login Successful");
        } catch (RuntimeException e) {
            throw e;
        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }
    }
}
