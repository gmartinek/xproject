package xproject.controller;

import java.util.Properties;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class JpaTestHelper {

    public static EntityManagerFactory emf;

    public static EntityManagerFactory getEntityManagerFactory() {
        if (emf == null) {
            Properties props = new Properties();
            props.setProperty("eclipselink.persistencexml", "META-INF/persistence-test.xml");
            emf = Persistence.createEntityManagerFactory("default", props);
        }

        return emf;
    }

    public static void resetDatabase() {
        EntityManager em = getEntityManagerFactory().createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        em.createNativeQuery("TRUNCATE SCHEMA public RESTART IDENTITY AND COMMIT NO CHECK")
                .executeUpdate();
        et.commit();
    }
}
